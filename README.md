# Currency Converter

- The deployed app is accessible [here](https://merhawifissehaye.gitlab.io/adyen-assignment-currency-converter/#/)
- The git repository is on gitlab [here](https://gitlab.com/merhawifissehaye/adyen-assignment-currency-converter/ )
- There are a couple of branches - one is a single converter on branch `single-currency-converter` (makes use of vuex heavily) and multiple converter is on master. But the master branch is my main submission.

- use `yarn dev` or `npm run dev` command to run locally after installing all packages by running `yarn` or `npm install`
