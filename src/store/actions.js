import * as types from './types'

export default {
  async getRates ({ commit, getters: { date } }) {
    commit(types.SET_LOADING)
    try {
      // TODO we can introduce some sort of caching here to prevent too much api calls
      // We can use localStorage to store the rates and save the date when the rates are fetched
      // Then only call the api every other day or on user action or something like that
      const { rates, base } = await this.$http.getRates(date)
      commit(types.UPDATE_CURRENCIES, Object.keys(rates).concat(base))
      commit(types.UPDATE_BASE_CURRENCY, base)
      commit(types.UPDATE_RATES, rates)
    } finally {
      commit(types.RESET_LOADING)
    }
  },

  async changeDate ({commit, dispatch}, date) {
    commit(types.SET_DATE, date)
    await dispatch('getRates')
  }
}
