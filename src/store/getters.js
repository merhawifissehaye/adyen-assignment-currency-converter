const dateTextToday = new Date().toISOString().slice(0, 10)

export default {
  currencies: state => state.currencies,
  baseCurrency: state => state.baseCurrency,

  // we are not using this getter
  // because we are generating a unique function in the action
  // everytime getRates is called.
  rates: state => state.rates,

  // When loading rates the Loading text shows
  loadingRates: state => state.loadingRates,

  date: state => state.date || dateTextToday,

  // The amount and currency on the left side
  leftAmount: state => state.leftAmount,
  leftCurrency: state => state.leftCurrency,

  // The amount and currency on the right side
  rightAmount: state => state.rightAmount,
  rightCurrency: state => state.rightCurrency
}
