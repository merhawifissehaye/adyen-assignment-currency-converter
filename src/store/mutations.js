import * as types from './types'

export default {
  [types.UPDATE_CURRENCIES] (state, currencies) {
    state.currencies = currencies
  },
  [types.UPDATE_BASE_CURRENCY] (state, currency) {
    state.baseCurrency = currency
  },
  [types.UPDATE_RATES] (state, rates) {
    state.rates = rates
  },
  [types.SET_LOADING] (state) { state.loadingRates = true },
  [types.RESET_LOADING] (state) { state.loadingRates = false },
  [types.SET_DATE] (state, date) { state.date = date }
}
