// The converter simply converts the source amount to the base currency
// and from base currency to the final amount
// it returns the result with only two decimal places showing
export default (rates, baseCurrency, fromAmount, fromCurrency, toCurrency) => {
  if (fromCurrency !== baseCurrency) {
    const inBaseCurrency = fromAmount / rates[fromCurrency]
    if (toCurrency === baseCurrency) return inBaseCurrency.toFixed(2)
    return (inBaseCurrency * rates[toCurrency]).toFixed(2)
  }
  return (fromAmount * rates[toCurrency]).toFixed(2)
}
