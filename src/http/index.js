import axios from 'axios'
export default {
  async getRates (date = 'latest') {
    const res = await axios.get(`https://api.exchangeratesapi.io/${date}`)
    return res.data
  }
}
