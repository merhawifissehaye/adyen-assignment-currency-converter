// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'
import './assets/css/main.scss'
import store from './store'
import http from './http'

Vue.config.productionTip = false

// This will make $http available to components and the store
Vue.prototype.$http = http
Vuex.Store.prototype.$http = http

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  // I am only using router to make use of <router-view>
  // It may be more useful as the app grows but for now
  // it's not really that useful and
  // TODO can be removed
  router,
  store,
  components: { App },
  template: '<App/>'
})
